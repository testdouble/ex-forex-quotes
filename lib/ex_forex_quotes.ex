defmodule ExForexClient do
  @moduledoc """
  This library is provided without warranty under the MIT license
  Created by Sam Jones <sam@testdouble.com>
  """

  def get_quotes(symbols) do
    symbols
    |> Enum.map(fn symbol ->
      {symbol, rate(symbol)}
    end)
    |> Map.new()
  end

  def convert(_from, to, quantity) do
    quantity * rate(to)
  end

  def rate(type) do
    api_key()
    start = type |> to_charlist() |> hd()
    start * (:rand.uniform() + 1)
  end

  def api_key do
    Application.get_env(:ex_forex_quotes, :api_key) ||
      raise """
      An API key must be configured in config.exs as:

      config :ex_forex_quotes, api_key: <API_KEY>
      """
  end
end
