# ExForexQuotes

Do money conversions with a pseudo real elixir package!

## Installation

```elixir
def deps do
  [
  {:ex_forex_quotes, git: "git@gitlab.com:testdouble/ex-forex-quotes.git"}
  ]
end
```

## Configuration

```elixir
config :ex_forex_quotes, api_key: <API_KEY>
```

## Get quotes for currencies

```elixir
ExForexClient.get_quotes([:USDJPY])
```

## Convert currencies

```elixir
ExForexClient.convert(:USD, :JPY, 100)
```

## License and Terms

This library is provided without warranty under the MIT license.
